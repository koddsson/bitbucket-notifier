# BitBucket notifier

> Browser extension that displays your BitBucket notifications unread count

Tries to shamelessly emulate the 
[github-notifier](https://github.com/sindresorhus/github-notifier) as much as
it can.

![](img/screenshot.png)

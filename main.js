(function() {
  'use strict';
  
  // Poll BitBucket for changes every 5 seconds.
  const POLLING_TIME = 5;
  
  document.addEventListener('DOMContentLoaded', function() {
    // Run the first update
    update();

    // Run update at a interval
    setInterval(update, POLLING_TIME * 1000);
  });
  
  /**
   * Check for notifications and update the badge text
   **/
  function update() {
    // Get the notifications HTML page
    fetch('https://bitbucket.org/account/notifications/', {
      // Pass in our current credentials
      'credentials': 'include'
    }).then(function(response) {
      // Get the HTML
      return response.text();
    }).then(function(text) {
      // Parse the HTML into a DOM tree
      return new DOMParser().parseFromString(text, "text/html");
    }).then(function(doc) {
      // Query the DOM tree for unread messages
      let unreadCount = doc.querySelectorAll('table tbody tr.unread').length;
      // Clear the badge if there are no unread messages; otherwise set the
      // badge text to the number of unread messages.
      chrome.browserAction.setBadgeText({
        text: unreadCount > 0 ? unreadCount.toString() : ''
      });
    });
  }
  
  /**
   * Add a event listener to the action of clicking the extension icon
   **/
  chrome.browserAction.onClicked.addListener(function (tab) {
    let notifTab = {
      url: 'https://bitbucket.org/account/notifications/'
    };
    if (tab.url === '' || tab.url === 'chrome://newtab/' || tab.url === notifTab.url) {
      // Open the notifications page in the current tab
      chrome.tabs.update(null, notifTab);
    } else {
      // Open the notifications page in a new tab
      chrome.tabs.create(notifTab);
    }
  });
})();
